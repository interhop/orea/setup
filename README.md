<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table des matières</summary>
  <ol>
    <li>
      <a href="#description-de-l-installation">Description de l'installation</a>
      <ul>
        <li><a href="#principe">Principe</a></li>
        <li><a href="#cas-dutilisation">Cas d'utilisation</a></li>
        <li><a href="#setups-de-base">Setups de base</a></li>
        <li><a href="#les-fichiers-de-configuration">Les fichiers de configuration</a></li>
      </ul>
    </li>
    <li>
      <a href="#déroulement">Déroulement</a>
      <ul>
        <li><a href="#prérequis">Prérequis</a></li>
        <li><a href="#personnalisation">Personnalisation</a></li>
        <li><a href="#utilisation">Utilisation</a></li>
      </ul>
    </li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>



<!-- INSTALLATION -->
## Description de l'installation

Vous trouverez dans ce répertoire tout le nécessaire pour installer rapidement Orea sur votre ordinateur (Linux) ou votre serveur.


### Principe

Cette méthode utilise [docker](https://www.docker.com/) et [docker-compose](https://docs.docker.com/compose/).

> Selon la firme de recherche sur l'industrie 451 Research, « Docker est un outil qui peut empaqueter une application et ses dépendances dans un conteneur isolé, qui pourra être exécuté sur n'importe quel serveur ». Il ne s'agit pas de virtualisation, mais de conteneurisation, une forme plus légère qui s'appuie sur certaines parties de la machine hôte pour son fonctionnement. Cette approche permet d'accroître la flexibilité et la portabilité d’exécution d'une application, laquelle va pouvoir tourner de façon fiable et prévisible sur une grande variété de machines hôtes, que ce soit sur la machine locale, un cloud privé ou public, une machine nue, etc.[^1]

[^1]: [https://fr.wikipedia.org/wiki/Docker_(logiciel)](https://fr.wikipedia.org/wiki/Docker_(logiciel))

Dit autrement, `Docker` suit un script (*Dockerfile*) qui lui permet de simuler une machine Linux isolée avec installé très strictement ce qui est indiqué comme nécessaire.

On peut ensuite lui faire construire une image qui contient tout le nécessaire à son projet, image qui pourra être mise en ligne, téléchargée, et exécutée par `docker` depuis une autre machine.

`docker-compose` permet, avec un simple fichier de configuration (*docker-compose.yaml*), de lancer plusieurs fois `docker` en même temps sur ce qu'on appelle des *container*, lui indiquant par exemple :
- différents scripts *Dockerfile* ou images
- des commandes machines à faire exécuter sur les machines crées
- un ordre d'exécution
- une ouverture réseau (ou non) sur chaque machine pour la communication
- des fichiers à ajouter aux *container* ou à partager entre plusieurs

### Cas d'utilisation

Ici, nous suggérons d'utiliser *docker-compose* sur une machine pour lancer en parallèle :
- le back-end Orea, réalisé avec [*django*](https://www.djangoproject.com/). Le script récupère l'image [stocké publiquement](https://hub.docker.com/r/interhoporg/orea-back), y ajoute des fichiers d'environnement (que vous adapterez), et lancera le serveur en accueillant les requêtes sur un port de la machine que vous aurez indiqué.
- (facultatif) si le front-end n'est pas utilisé, un serveur *nginx* pour diriger les requêtes d'un port de la machine (que vous aurez indiqué) vers le *container* du back-end
- le front-end Orea, réalisé avec [*react.ts*](https://react.dev/). Le script récupère l'image [stocké publiquement](https://hub.docker.com/r/interhoporg/orea-front), contenant les fichiers html/js générés et lancera [nginx](https://nginx.org/) pour diriger les requêtes vers l'interface utilisateur et rediriger les requêtes *api* vers le *container* du back-end
- (facultatif) un système de base de données [postgresql](https://www.postgresql.org/) pour y créer une base de données dans laquelle le back-end pourra stocker les données nécessaires à Orea.

### Setups de base

4 fichiers *docker-compose.yaml* figurent dans la branche *main* de ce répertoire. Elles correspondent à 4 installations préparées : 
- back_only:
  - [docker-compose.yaml](https://framagit.org/interhop/orea/setup/-/blob/main/back_only/docker-compose.yaml) : lance uniquement le serveur back-end. **Nécessite** de le connecter à une base de données existante.
Utilise 
  - [with_db/docker-compose.yaml](https://framagit.org/interhop/orea/setup/-/blob/main/back_only/with_db/docker-compose.yaml) : lance le serveur back-end connecté à une base de données Postgresl. **Sauf changement**, la base de données sera réinitialisée avec une base de démo.
Utilise 
  - Le bon fonctionnement peut-être vérifié via (remplacer par le port adéquat) : http://localhost:8081/accounts/login puis http://localhost:8081/docs
- back_and_front:
  - [docker-compose.yaml](https://framagit.org/interhop/orea/setup/-/blob/main/back_and_front/docker-compose.yaml) : lance le serveur back-end et un serveur nginx muni du rendu html du front-end. **Nécessite** de le connecter à une base de données existante.
Utilise 
  - [with_db/docker-compose.yaml](https://framagit.org/interhop/orea/setup/-/blob/main/back_and_front/with_db/docker-compose.yaml) : lance le serveur avec front-end et le serveur back-end connecté à une base de données Postgresl. **Sauf changement**, la base de données sera réinitialisée avec une base de démo.
Utilise 
  - Le bon fonctionnement du front-end peut-être vérifié via (remplacer par le port adéquat) : http://localhost:3002/
  - Le bon fonctionnement du back-end peut-être vérifié via (remplacer par le port adéquat) : http://localhost:8081/accounts/login puis http://localhost:8081/docs, ou bien http://localhost:3002/api/docs

Si la base de donnée de démo est initialisée, la première connexion peut se faire avec le username *ckramer* et un mot de passe quelconque.

### Les fichiers de configuration

[demo.env](https://framagit.org/interhop/orea/setup/-/blob/main/demo.env) ou [example.env](https://framagit.org/interhop/orea/setup/-/blob/main/example.env) : variables d'environnement lues par le script python du back-end.

[example.conf_auth.py](https://framagit.org/interhop/orea/setup/-/blob/main/example.conf_auth.py) : permet de spécifier, pour le *back-end* des fonctions personnalisées pour authentifier un utilisateur.

[demo.docker-entrypoint.sh](https://framagit.org/interhop/orea/setup/-/blob/main/demo.docker-entrypoint.sh) ou [example.docker-entrypoint.sh](https://framagit.org/interhop/orea/setup/-/blob/main/example.docker-entrypoint.sh) : script unix qui sera exécuté par *docker* dans l'image du back-end. La version *demo* a une ligne de plus indiquant la réinitialisation de la base de données.

[demo.default.conf](https://framagit.org/interhop/orea/setup/-/blob/main/back_only/demo.default.conf) ou [front.nginx.conf](https://framagit.org/interhop/orea/setup/-/blob/main/back_and_front/front.nginx.conf) : configurations pour les serveurs *nginx*

[example.env.js](https://framagit.org/interhop/orea/setup/-/blob/main/back_and_front/example.env.js) : variables d'environnement lues par l'interface front-end

<!-- GETTING STARTED -->

## Déroulement

La description suivante requiert d'utiliser une machine Linux.

### Prérequis

* Python
  ```sh
  sudo apt-get update
  sudo apt-get install python3.10 -y
  ```

* docker
  ```sh
  sudo apt install docker docker-compose -y
  ```

### Personnalisation

#### Pour déployer en production

Dans le cas *with_db*, sécurisez l'accès à la base de données en modifiant dans *docker-compose.yaml*:
- *POSTGRES_DB*=
- *PGUSER*=
- *POSTGRES_USER*=
- *POSTGRES_PASSWORD*=
- adaptez la partie gauche de `ports` selon le.s port.s mis à disposition sur votre machine (dans "8081:8080", remplacez `8081`)
- changez *demo.docker-entrypoint* pour *example.docker-entrypoint* (pour faire sans la ligne `python manage.py reset_demo`)

Modifiez dans *demo.env* ou *example.env*:
- IS_LOCAL_SERVER=FALSE
- ENVIRONMENT='prod'
- SECRET_KEY= générez un mot de passe/une clé complexe
- POSTGRES_
- pour permettre aux utilisateurs de voter pour des ajouts sur le répertoire officiel, demandez un VOTING_PRIVATE_TOKEN
- assurez-vous que `DEMO_USER_USERNAME=''` (ou que la ligne n'existe pas)


#### Dans docker-compose.yaml

Pour utiliser une version particulière du front-end ou back-end : modifiez `image: interhoporg/orea-back:x.x.x`

Si vous n'utilisez pas une configuration *with_db*, et que la base de données visée n'est pas sur la machine même : remplacez `host-gateway` par l'adresse de la base de donnée


#### Authentification

Actuellement, il n'y a pas de processus d'inscription d'utilisateur mis en place. Pour connecter la phase de validation de mot de passe ou de token de connexion à un serveur dédié, ce fichier doit être adapté.

Sans modification, le back-end va lui-même gérer les comptes utilisateurs et les sessions avec le protocole [OAuth2](https://oauth.net/2/) (*access token* et *refresh token* lus et insérés via les cookies).

Pour la création des comptes par le back-end, assurez-vous que dans *example.env*: `ALLOW_ACCOUNT_MANAGEMENT=True`

<u>La création de compte n'est pas encore fournie dans le front-end.</u>


#### Cas avec base de données locale

Si vous n'utilisez pas une configuration *with_db*, et la base de donnée sur la même machine, vous aurez peut-être besoin de modifier :

- la variable `listen_address` (pour '*' par exemple) dans */etc/postgresql/{pg_version}/main/postgresql.conf*
- la ligne *IPv4 local connections* de */etc/postgresql/{pg_version}/main/pg_hba.conf*


### Utilisation

#### Lancement

Depuis le dossier principal :

```sh
chmod +x example.docker-entrypoint.sh
```

Choisissez le *docker-compose.yaml* désiré et, depuis le terminal, placez-vous dans son dossier.

```sh
docker-compose up --build
```

Si vous le lancez après un arrêt et voulez vous assurer de relancer comme la première fois :

```sh
docker-compose down --volumes && docker-compose up --build
```
(cela effacera le cache)

#### Pérennisation

Afin de ne pas remplacer vos réglages lors de la mise à jour du répertoire local, pensez à vous créer un dossier indépendant avec des copies des fichiers voulus et paramétrés.


<!-- CONTACT -->
## Contact

Alexandre Martin, contributeur principal - [@w848](https://framagit.org/w848)

[Interhop.org](Interhop.org)
