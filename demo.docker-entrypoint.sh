#!/usr/bin/env bash
set -e

python manage.py migrate --noinput
python manage.py collectstatic --noinput
python manage.py reset_demo

gunicorn orea.wsgi:application --bind 0.0.0.0:8000
